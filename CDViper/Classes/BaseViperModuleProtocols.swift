//
//  BaseViperModuleProtocols.swift
//  CDViper
//
//  Created by Coscodan Dinu on 11/16/18.
//

import Foundation


public protocol ViperModuleInput : class {
    func setOutput(output:ViperModuleOutput)
}

public extension ViperModuleInput {
    func setOutput(output:ViperModuleOutput) {}
}

public protocol ViperModuleOutput : class {}

public protocol ViperModuleConfigurator {
    func configure(with viewInput:UIViewController)
}
