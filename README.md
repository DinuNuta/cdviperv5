# CDViper

[![CI Status](https://img.shields.io/travis/DinuNuta/CDViper.svg?style=flat)](https://travis-ci.org/DinuNuta/CDViper)
[![Version](https://img.shields.io/cocoapods/v/CDViper.svg?style=flat)](https://cocoapods.org/pods/CDViper)
[![License](https://img.shields.io/cocoapods/l/CDViper.svg?style=flat)](https://cocoapods.org/pods/CDViper)
[![Platform](https://img.shields.io/cocoapods/p/CDViper.svg?style=flat)](https://cocoapods.org/pods/CDViper)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CDViper is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CDViper'
```

## Author

DinuNuta, coscodan.dinu@gmail.com

## License

CDViper is available under the MIT license. See the LICENSE file for more info.
